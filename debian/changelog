manimpango (0.6.0-2) unstable; urgency=medium

  * Update d/watch and ignore stale 1.0.0~a releases
  * Build documentation with default Python

 -- Timo Röhling <roehling@debian.org>  Sat, 05 Oct 2024 21:08:29 +0200

manimpango (0.6.0-1) unstable; urgency=medium

  * New upstream version 0.6.0
    - Add Python 3.13 support (Closes: #1081571)
  * Bump Standards-Version to 4.7.0
  * Migrate Depends: pkg-config to Depends: pkgconf

 -- Timo Röhling <roehling@debian.org>  Sun, 15 Sep 2024 15:44:26 +0200

manimpango (0.5.0-1) unstable; urgency=medium

  * Handle pre-release versions in d/watch
  * New upstream version 0.5.0
  * Use Furo theme for documentation
  * Bump Standards-Version to 4.6.2

 -- Timo Röhling <roehling@debian.org>  Mon, 13 Nov 2023 17:36:59 +0100

manimpango (0.4.3-2) unstable; urgency=medium

  * Use autopkgtest-pkg-pybuild

 -- Timo Röhling <roehling@debian.org>  Fri, 09 Dec 2022 17:13:12 +0100

manimpango (0.4.3-1) unstable; urgency=medium

  * New upstream version 0.4.3

 -- Timo Röhling <roehling@debian.org>  Sun, 27 Nov 2022 21:17:10 +0100

manimpango (0.4.2-1) unstable; urgency=medium

  * New upstream version 0.4.2
  * Enable hardening flags

 -- Timo Röhling <roehling@debian.org>  Mon, 21 Nov 2022 20:16:04 +0100

manimpango (0.4.1-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Contact, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 29 Aug 2022 15:04:14 +0100

manimpango (0.4.1-3) unstable; urgency=medium

  * Properly clean source tree after build (Closes: #1015097)

 -- Timo Röhling <roehling@debian.org>  Mon, 25 Jul 2022 18:07:21 +0200

manimpango (0.4.1-2) unstable; urgency=medium

  * Source only upload.

 -- Timo Röhling <roehling@debian.org>  Sat, 25 Jun 2022 16:03:33 +0200

manimpango (0.4.1-1) unstable; urgency=medium

  * Initial release (Closes: #1012605)

 -- Timo Röhling <roehling@debian.org>  Fri, 10 Jun 2022 10:01:46 +0200
